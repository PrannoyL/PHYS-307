import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)

### Constants ###
P   = 40 # Power
m   = 25  # Mass

### Time Parameters ###
dt  = 1   # Time Width
t_f = 30 # Final Time
t_n = [0] # Time Array

### Velocity Parameters ###
v_0 = 1    # Initial Velocity
v_n = [v_0] # Velocity Array


def update_velocity(t_n, v_n):
    # Equation 1
    # Equation 2
    return True if A??? > B??? else update_velocity(t_n, v_n) ## Ternary operator!

update_velocity(t_n, v_n)

### Print Plots ###
plt.title('Velocity v. Time', fontsize=18)
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Velocity (m/s)', fontsize=12)

plt.plot(t_n, v_n)

ax.set_ylim(bottom=v_0)
ax.set_xlim(left=0)
plt.show()
