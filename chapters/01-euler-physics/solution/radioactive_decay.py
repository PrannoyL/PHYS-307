import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)

### Time Parameters ###
t_half   = 5700 # Half-Life
tau      = t_half / np.log(2) # Decay Constant
dt       = 100 # Time Step Width
T_n      = [0] # Time Step Array


### Carbon 14 Parameters ###
C14     = 1000  # Initial Population
C14_rem = .1    # C14 Remaining Cutoff
C14_n   = [C14]    # C14 Population Array

### Function to Update Population ###
def decay(C14_n, T_n):

    C14_n.append(C14_n[-1]*(1 - dt/tau))
    T_n.append(T_n[-1] + dt)

    if C14_n[-1] < C14_rem:
        return
    else:
        decay(C14_n, T_n)


decay(C14_n, T_n)

### Print Plots ###
plt.title('C14 v. Time', fontsize=18)
plt.xlabel('Time (yr)', fontsize=12)
plt.ylabel('C14 (kg)', fontsize=12)

plt.plot(T_n,C14_n)

ax.set_ylim(bottom=0)
ax.set_xlim(left=0)
plt.show()
