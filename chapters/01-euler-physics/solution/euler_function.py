import numpy as np

### Input Parameters ###
N        = 100  # Initial value of N
dt       = 1  # Change in Time
alpha    = .1 # Decay Constant


### Function to update N ###
def next_N(N, dt, alpha):
    return N * (1 - alpha) * dt


### Test by outputting to terminal ###
while N > .01:
    N = next_N(N, dt, alpha)
    print(round(N,3))
