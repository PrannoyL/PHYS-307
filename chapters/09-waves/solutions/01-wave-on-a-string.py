import sys, math, random
import numpy as np
import random
import matplotlib.pyplot as plt
fig, axs = plt.subplots(1)

sys.setrecursionlimit(10**9)


def triangle_wave(L):
    return random.sample(range(L), L)

def gauss_wave(n_L, x_0, A, alpha):
    y_0=[]
    for l in range(n_L-1):
        y_0.append(A * np.exp(- alpha * ( l - x_0 )**2))

    return y_0

def rand_wave(L):
    return random.sample(range(L), L)

def main():

    # Plot modulus and repititions
    n_plot = 10
    N = 100

    # Infinitesimals #
    dt, dx = [ .01, .01 ]
    c = 1
    r = c * dt / dx


    # From dx
    n_L = int(1/dx)

    # Or Count
    #n_L = int(1/dx)

    # Set Lenth and intervals
    L_min, L_max = [ 0, 10 ]
    L_arr = [ L * dx for L in range(n_L-1) ]

    ## Gauss parameters ##
    A = 1
    alpha = .001
    x_0 = 50

    # Create Initial Plots
    y_0 = gauss_wave(n_L, x_0, A, alpha) #random.sample(range(L), L)
    y_1 = y_0
    plt.plot(L_arr, y_0)

    def propagate(u_0, u_1):
        u_2=[0]
        for i in range(n_L-2):

            print(i)
            u_2.append(2*(1-r**2)*u_1[i] - u_0[i] + r**2 * (u_1[i-1] + u_1[i+1]))

        return u_1, u_2

    for n in range(N):
        print(n)
        y_0, y_1 = propagate(y_0, y_1)
        if n % n_plot == 0:
            plt.plot(L_arr, y_1)


    plt.title('Wave on String', fontsize=18)
    plt.xlabel('X', fontsize=12)
    plt.ylabel('Y', fontsize=12)

    plt.show()

main()
