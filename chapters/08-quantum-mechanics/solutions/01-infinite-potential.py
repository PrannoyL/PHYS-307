import numpy as np
import matplotlib.pyplot as plt
import math
import random

fig, axs = plt.subplots(1)
L = 1
dL = .1
N =  np.rint(1.5 * L).astype(np.int32)

n = 1
V = 0
even_psi = [ 1, 1 ]
#odd_psi = [ 0 0 ]

E = (np.pi ** 2) / 8
#E = 0

k_p = (2 * n - 1)/(2 * L) * np.pi
k_m = 2 * np.pi / L


def update_wavefunction(psi, l):
    #V = 10 if l >= L else 0
    psi.append(2 * ( 1 -  dL ** 2 * (E - V)) * psi[-1] - psi[-2] )
    return psi if l>= N else update_wavefunction(psi, l+dL)

even_psi=update_wavefunction(even_psi, 0)
x_arr=[i * dL   for i in range(len(even_psi))]

plt.plot(x_arr, even_psi)

#fig.suptitle('Poincare Section', fontsize=18)
#
#plt.xlabel('Temperature', fontsize=12)
#plt.ylabel('Average Spin', fontsize=12)

#
plt.axhline(y=0, color='r', linestyle='-')
plt.axvline(x=L, color='r', linestyle='-')
plt.show()
def main():
    L = 1
    dL = .1

    x_arr=[0, 0]
    even_psi = [ 1, 1 ]

    n = 100
    N =  np.rint(1.5 * L).astype(np.int32)
    k_p = (2 * n - 1)/(2 * L) * np.pi
    k_m = 2 * np.pi / L

    E_0 = (np.pi ** 2) / 8
    delta_E = .01

    V_inf = 10
    V = 0

    delta_R_min = .000001

    count = 1
    converged=False
    direction=1

    E = E_0
    def next_even_psi(psi, l):

        if  l <= L:
            V = 0
        else:
            V = V_inf
        psi.append(2 * ( 1 -  dL ** 2 * (E - V)) * psi[-1] - psi[-2] )
        psi.insert(0,psi[-1])
        x_arr.append(len(psi)* dL)
        x_arr.insert(0, -1 * len(psi) * dL)

        return psi if l>= N else next_even_psi(psi, l+dL)

    while not converged:
        x_arr=[0, 0]
        even_psi = [ 1, 1 ]
        E = E_0 + delta_E * count * direction
        even_psi=next_even_psi(even_psi, 0)

        ## Calc delta_R
        for i in range(len(even_psi)):
            delta_R = np.abs(1 - np.sqrt( even_psi[i-1] ** 2 + x_arr[i] ** 2 ))

            print(delta_R)
            if delta_R < delta_R_min:
                converged = True
                break
        direction *= - 1
        count += 1

    plt.plot(sorted(x_arr), even_psi)
    plt.grid(color='grey', linestyle='--', linewidth=.5)
    plt.axhline(0, color='black', linewidth=.5)
    plt.axvline(0, color='black', linewidth=.5)
    plt.axhline(y=0, color='r', linestyle='dotted')
    plt.axvline(x=L, color='r', linestyle='dotted')
    plt.show()

main()
