import numpy as np
import matplotlib.pyplot as plt
import math
import random

fig, axs = plt.subplots(1)

L = 1
dL = 0.01
N = 1 #num steps for loop

n = 1 #energy state
V = 0
even_psi = [ 1, 1 ]
#odd_psi = [ 0 0 ]

E_target = (np.pi**2) / 8

E = 1
dE = 0.01
E_steps = 100


k_p = (2 * n - 1)/(2 * L) * np.pi
k_m = 2 * np.pi / L


def update_wavefunction(psi, l):
    new_psi = 2*psi[-1] - psi[-2] - 2*((dL)**2)*(E - V)*psi[-1]
    psi.append(new_psi)
    return psi if l>= N else update_wavefunction(psi, l+dL)

for i in range(E_steps):
    even_psi = update_wavefunction(even_psi, 0)

even_psi=update_wavefunction(even_psi, 0)
x_arr=[dL*i for i in range(len(even_psi))]
    return psi if l>= N else update_wavefunction(psi, l+dL)

even_psi=update_wavefunction(even_psi, 0)
x_arr=

plt.plot(x_arr, even_psi)

#fig.suptitle('Poincare Section', fontsize=18)
#
#plt.xlabel('Temperature', fontsize=12)
#plt.ylabel('Average Spin', fontsize=12)

#
plt.axhline(y=0, color='r', linestyle='-')
plt.axvline(x=L, color='r', linestyle='-')
plt.show()
