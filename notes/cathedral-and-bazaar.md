# The Cathedral and the Bazaar

## A Brief History of Hackerdom
## The Cathedral and the Bazaar
**Cathedral**: Collaborative programming environment that is 'perfect', engineered, and 'beautiful'
**Bazaar**: Collaborative programming environment where information is shared and traded chaotically, but establishes a cohesive project.

Words of wisdom concerning the bazaar:

1. Every good work of software starts by scratching a developer's personal itch.
1. Good programmers know what to write. Great ones what to rewrite and reuse
1. Plan to throw one away; you will anyhow
1. If you have the right attitude, interesting problems will find you
1. When you lose interest in a program, your last duty to it is to hand it off the a competent successor
1. Treating your users as co-developers is your least hassle route to rapid code improvement and effective debugging
1. Release early. Release often.  And listen to your customers.
1. Given a large enough beta-tester and co-developer base, almost every problem will be characterized quickly and the fix obvious to someone
1. Smart data structures and dumb code works a lot better than the other way around
1. If you treat your beta-testers as if they're your most valuable resource, they will respond by becoming your most valuable resource
1. Often, the most striking and innovated solutions come from realizing that your concept of the problem was wrong
1. The next best thing to having good ideas is recognizing good ideas from your users. Sometimes, the latter is better.
1. Any tool should be useful in the expected way, but a truly great tool lens itself to uses you never expected
1. When writing gateway software of any kind, take pains to disturb the data stream as little as possible. and never throw away information unless the recipient forces you
1. When your language is nowhere near Turing complete, syntactic sugar can be your friend
1. A security system is only as secure as its secret. Beware of pseudo-secrets
1. To solve an interesting problem, start by finding a problem that interests you
1. Provided the development coordinator has a communications medium at least as good as the internet, and knows how to lead without coercion, many heads are inevitably better than one.

## Homesteading the Noosphere
## The Magic Cauldron
## Revenge of the Hackers
## Beyond Software
## How to Become a Hacker
